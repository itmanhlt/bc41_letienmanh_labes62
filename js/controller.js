export let renderListGlass = (arr) => {
  let contentHTML = "";
  for (let i = 0; i < arr.length; i++) {
    let content = `<button id="${arr[i].id}" class="btn glass_click col-4"><img style="width:100%" src="${arr[i].src}" alt="" /></button>`;
    contentHTML += content;
  }
  document.getElementById("vglassesList").innerHTML = contentHTML;
};

export let renderGlassToAvatar = (arr) => {
  let btnList = document.querySelectorAll(".glass_click");
  for (let i = 0; i < btnList.length; i++) {
    btnList[i].onclick = () => {
      let contentHTML = "";
      let contentInfoHTML = "";
      arr.forEach(function (glasses) {
        if (glasses.id == btnList[i].id) {
          let content = `<div class="glass_checked"><img src="${glasses.virtualImg}" alt="" /></div>`;
          let contenInfo = `<div class="glass_checked"><h5>${glasses.brand} - ${glasses.name} (${glasses.color})</h5>
          <button class="btn btn-danger">${glasses.price}</button>
           <span class="text-success">Stocking</span>
           <p>${glasses.description}</p></div>`;
          contentHTML += content;
          contentInfoHTML += contenInfo;
          document.getElementById("avatar").innerHTML = contentHTML;
          document.getElementById("glassesInfo").innerHTML = contentInfoHTML;
          document.querySelector(".vglasses__info").style.display = "block";
        }
      });
    };
  }
};

export let removeGlasses = (check) => {
  var divList = document.querySelectorAll(".glass_checked");
  divList.forEach((item) => {
    if (check == false) {
      item.style.display = "none";
      document.querySelector(".vglasses__info").style.display = "none";
    } else {
      item.style.display = "block";
      document.querySelector(".vglasses__info").style.display = "block";
    }
  });
};
